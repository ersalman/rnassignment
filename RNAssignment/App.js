/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppState,
  StyleSheet,
  Button,
  View
} from 'react-native';

import PushNotification from 'react-native-push-notification';

export default class App extends Component {

constructor(props) {
  super(props);

  this.handleAppStateChanged = this.handleAppStateChanged.bind(this);

  this.state = {
    second: 2
  }
}

componentDidMount() {
  AppState.addEventListener('change', this.handleAppStateChanged);
}

componentWillMount() {
  AppState.removeEventListener('change', this.handleAppStateChanged);
}


  handleLocalNotification() {
    PushNotification.configure({
      onNotification: function(notification) {
      }
    })
  }

  handleAppStateChanged(appState) {
    
    PushNotification.localNotificationSchedule({
      message: "Local Notification Message", // (required)
      date: new Date(Date.now() + (this.state.second * 1000)) // in 2 secs
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title = 'Notify'
        onPress = {this.handleLocalNotification}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
